# Server (flow-fetch)

A wrapper for the Fetch API to simplify jobs and error handling. 
Implements [`Console`](https://bitbucket.org/johnsonjzhou/flow-utils/) 
for error handling.  

Future work:  
- Integrate `flow-idbworker` and `flow-serviceworker` for offline POST handling. 

---
## Version  
1.0.3 [Changelog](./doc/changelog.md)  

---
## Install  

````bash
npm install https://bitbucket.org/johnsonjzhou/flow-fetch.git
````

---
## Use  

````javascript
import Server from 'flow-fetch';
````

---
## Job object  
`Server` keeps track of all server Jobs by way of an `array` of Job objects. 
This is the `object` used within this context to describe one server Job.  

### Parameters

***cid*** `string`  
The unique identifier for the Job.  

***timestamp*** `string`  
The unix timestamp on which the Job was last updated.  

***method*** `string`  
The Fetch request method. Supports `get`, `post`, `update`.  

***path*** `string`  
The `url` of the Fetch request method.  

***request***  `object`  
The data to be used for the Fetch event in the form of an `object`. 
By default, the content type of the request will be set to `application/json`. 
However, if the object is an instance of `FormData`, then the browser will 
set it to `multipart/form-data`.  

***response***  `object`  
Stores the Fetch response as a custom object. See **Response object**.  

***status***  `int`  
The status code of the server Job. See **Job status codes**.  

***callback*** `function`  
The callback function to invoke when the Job is complete, which then 
applies the `cid` as an argument. The callback then can retrieve the 
Job object by invokinig `Server.retrieveResponse` with the `cid`.  

### Job status codes

***860***  
Job has been created and is ready and waiting to start.  

***861***  
Job is in progress.  

***862***  
Job is complete. If an error occured during the Fetch event, the Job will 
still be marked as complete. Refer to the `Response` status codes for details.  

---
## Response object  
A standardised object describing the Fetch response for all Jobs.  

***ok*** `bool`  
Whether the Job succeeded. Similar to `Response.ok` in the Fetch API.  

***status*** `int`  
The Response status code.  

***data*** `mixed`  
The data returned from the Fetch event. Currently supported content types are: 
`application/json`, `text/*` and `Blob`.  

### Response status codes
All Response status codes are passed through as is from the Fetch event. 
The following exceptions apply:  

***408: Request Timeout***  
Fetch errors involving network connectivity (`TypeError` or `AbortError`) 
are handled with a `Response` status of 408.  

***410: Gone***  
Means the Job cannot be found. Usually this means either an invalid `cid` 
was requested or the Job has been deleted.  

---
## Methods
- [Server.createJob](#server.createjob)  
- [Server.startJob](#server.startjob)  
- [Server.retrieveResponse](#server.retrieveresponse)  
- [Server.purgeJobs](#server.purgejobs)  
- [Server.registerErrorHandler](#server.registererrorhandler)  

---
## Server.createJob  
Creates a new server Job and starts it immediately.  

### Parameter(s)  

***method*** `string`  
The Fetch request method. Supports `get`, `post`, `update`.  

***path*** `string`  
The `url` of the Fetch request method.  

***data*** `object`  
*Optional*. The data to be used for the Fetch event in the form of an `object`.  

***callback*** `function`  
*Optional*. The callback function to invoke when the Job is complete. 

### Return value(s)  
The `cid` as a `string`.

### Exceptions  
`ServerQueueError`  
- Unknown server method, or  
- Unknown request path.

---
## Server.startJob  
Starts the server Job to the corresponding `cid`. 
Can also be used to restart a Job that is in completed state.  

### Parameter(s)  

***cid*** `string`  
The `cid` of the Job.  

### Return value(s)  
Void.  

### Exceptions  
`ServerQueueError`  
- An invalid server job id was provided, or  
- Could not find a server job with given id.  

---
## Server.retrieveResponse
Retrieve the response of a particular server job identified by `cid`. 
Usually used in the `callback` to handle the server response.  

### Parameter(s)  

***cid*** `string`  
The `cid` of the Job.  

### Return value(s)  
`object`  
The **Response object**.  

### Exceptions  
`ServerQueueError`  
- An invalid server job id was provided.  

---
## Server.purgeJobs
Purge one or all server jobs. Provide a `cid` to only purge that particular job.  

### Parameter(s)  

***cid*** `string`  
*Optional*. The `cid` of the Job.  

### Return value(s)  
Void.

### Exceptions  
`ServerQueueError`  
- An invalid server job id was provided.  

---
## Server.registerErrorHandler
Register an error handler to catch error events that bubble. Useful for 
setting application status, error notifications, etc. This extends 
the basic error handling provided by `Console`.  

### Parameter(s)  

***handler*** `function`  
The handler function to register, which will receive the `Error` object as 
the argument.  

### Return value(s)  
Void.

---
## Examples  

````javascript
// initiate constructor
window.Server = new Server();

// create a 'get' Job
window.Server.createJob({
  method: 'get', 
  path: '/api/readonly/', 
  callback: (cid) => {
    // do something
  }
});

// create a 'post' Job
window.Server.createJob({
  method: 'post',
  path: '/api/login/', 
  data: { username: 'user', password: '1234' },
  callback: (cid) => {
    // get the response
    const response = window.Server.retrieveResponse(cid);

    const {
      ok, 
      status, 
      data
    } = response;

    // do something
  }
});

// handle errors
window.Server.registerErrorHandler((error) => {
  (error instanceof ServerQueueError) && (app.error = true);
});

````

---
## License
MIT.  
Johnson Zhou [johnson@simplyuseful.io](mailto://johnson@simplyuseful.io).  