# `flow-fetch` changelog  

## 1.0.4 
Post method will return content rather than null when non success response.  

## 1.0.3 
Expanded to update, put and delete methods  
Fetch method is handled in uppercase.  

## 1.0.2  
Response content-type use regex to match  

## 1.0.1  
Fix import error from `flow-utils`  

## 1.0.0  
Initial commit  