/**
 * Server communication functions
 * @author Johnson Zhou <johnson.simplyuseful.io>
 * 
 * @export  Server
 * 
 * @example Server Job status codes
 * 860: ready to start
 * 861: in progress
 * 862: complete
 * 
 * @example Job object
 * {callback, cid, method, path, request, response, status, timestamp}
 * 
 * @example offline fetch response, 408: client error, request time out
 * { status: 408, data: null, ok: false }
 * 
 * @example job not found response, 410: client error, resource gone
 * { status: 410, data: null, ok: false }
 * 
 * @method  createJob
 * @method  startJob
 * @method  purgeJobs
 * @method  retrieveResponse
 * @method  registerErrorHandler
*/
import ServerQueueError from './server_queue_error';
import { Console, randKey, unixTimestamp } from 'flow-utils';

class Server {

  constructor() {

    // Server Jobs
    this.jobs = [];       // array of Job objects

    // console
    this.Console = new Console('Server', 'goldenrod');

    // setters
    this.createJob = this.createJob.bind(this);
    this.registerErrorHandler = this.registerErrorHandler.bind(this);
    this.purgeJobs = this.purgeJobs.bind(this);

    // getters 
    this.retrieveResponse = this.retrieveResponse.bind(this);
    
    // handlers
    this.get = this.get.bind(this);
    this.post = this.post.bind(this);
    this.startJob = this.startJob.bind(this);
    this.handleJobs = this.handleJobs.bind(this);
    this.handleResponse = this.handleResponse.bind(this);
    this.handleFetchError = this.handleFetchError.bind(this);

    // show confirmation to console
    this.Console.log('Server initialised');
  }

  /**
   * Register an error handler to be used with Console in try/catch events
   * in Console
   * @param  {function}  handler
   */
  registerErrorHandler(handler) {
    this.Console && this.Console.registerErrorHandler(handler);
  }

  /**
   * Gets a url using fetch()
   * @param  string  url
   * @return  {promise}  {status, data, ok}
   */
  get(url) {
    return fetch(url)
    .then((response) => {
      const {
        ok,
        headers,
        status,
      } = response;

      const contentType = headers.get('Content-Type');

      // if the response is not ok, send null data
      if (!ok) return { status, data:null , ok };

      // json
      if (ok && contentType.match(/json/))
      return response.json()
      .then(json => ({ status, data: json, ok }));

      // text
      if (ok && contentType.match(/text/))
      return response.text()
      .then(text => ({ status, data: text, ok }));

      // everything else
      return response.blob()
      .then(blob => ({ status, data: blob, ok }));
    })
  }

  /**
   * Posts form data using fetch(), 
   * but can also be used other modifying REST methods  
   * @param  {string}  url
   * @param  {FormData|object}  data
   * @param  {object}  headers
   * @example  {data:{name: value}}
   *
   * @return  {promise}  {status, data, ok}
   * 
   * @note data - will only data if content-type is application/json
   */
  post(url, data = {}, headers = {}, method = 'post') {
    // check the origin of data
    // if data is not constructed using FormData, but a standard object
    // change headers to JSON and turn data into JSON
    // headers will need to omitted if we are using FormData class
    // browser will add Content-Type: multipart/form-data
    const dataType = typeof data;

    // do nothing if data is not an object
    if (dataType !== 'object') return;

    // set headers to application/json,
    // if we are not not handling FormData
    !(data instanceof FormData) &&
    (data = JSON.stringify(data)) && 
    (headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    });

    return fetch(url, {
      method,
      headers,
      body: data,
    })
    .then((response) => {
      const {
        ok,
        headers,
        status,
      } = response;

      const contentType = headers.get('Content-Type');

      // json
      if (contentType.match(/json/))
      return response.json()
      .then(json => ({ status, data: json, ok }));

      // text
      if (contentType.match(/text/))
      return response.text()
      .then(text => ({ status, data: text, ok }));

      // everything else
      return response.blob()
      .then(blob => ({ status, data: blob, ok }));
    });
  }

  /**
   * Creates a new server Job
   * @param {string} fetchMethod - get* || post || patch || put || delete 
   * @param {string} path 
   * @param {object} data 
   * @param {function} callback 
   * @param {object} headers 
   *
   * @return  {string}  cid  (true)
   * 
   * @throws  {ServerQueueError}
   */
  createJob(fetchMethod = 'get', path = '', data = null, callback = null, headers= {}) {
    try {

      const method = fetchMethod.toUpperCase();

      if (
          !((method === 'GET') ||
          (method === 'POST') ||
          (method === 'PATCH') || 
          (method === 'PUT') || 
          (method === 'DELETE')) 
        )
        throw new ServerQueueError('Unknown server method.');

      if (path === '')
        throw new ServerQueueError('Unknown request path.');

      if (!data) 
      this.Console.log('WARNING: Data not supplied', 'Create Job');

      if (!callback) 
      this.Console.log('WARNING: Callback not supplied', 'Create Job');

      let cid = randKey(24);
      let timestamp = unixTimestamp();

      this.jobs.push({
        cid: cid,
        path: path,
        method: method,
        request: data,
        response: null,
        status: 860,
        timestamp: timestamp,
        callback: callback, 
        headers: headers, 
      });

      this.handleJobs(cid);
      return cid;

    } catch (error) {
      this.Console.handleError(error);
    }
  }

  /**
   * Checks all server Jobs for 'queued' jobs then start them
   */
  handleJobs() {
    
    try {

      this.jobs.forEach(job => {
        // 860 = queued
        if (job.status === 860) {
          this.startJob(job.cid);
        }
      });

    } catch (error) {
      this.Console.handleError(error);
    }
  }

  /**
   * Handles fetch response
   * @param  {object}  responseNext: {status, data}
   * @param  {int}  index - index of this.jobs
   */
  handleResponse(responseNext, index) {
    let {
      cid, 
      response, 
      status, 
      timestamp, 
      callback, 
      ... rest
    } = this.jobs[index];

    response = responseNext;
    status = 862; // 862 = done
    timestamp = unixTimestamp(); 

    this.jobs[index] = {cid, response, status, timestamp, callback, ...rest};
    this.Console.log(response, `Response: ${cid}`);

    callback && callback(cid);
  }

  /**
   * Handles an error for a server job through fetch,
   * calls this.handleResponse if network offline with dummy response
   * or notifies bugsnag if something else
   * @param  {Error}  error - instance of AbortError / TypeError
   * @param  {int}  index - index of this.jobs
   * 
   * @note fetch only rejects on network error
   * if we get an AbortError or TypeError (offline),
   * then we will pass through a dummy response to the callback
   * with status of 408 and null data
   */
  handleFetchError(error, index) {
    const offline = (
      error instanceof TypeError || 
      error.constructor.name === 'AbortError'   // AbortError is not defined
    );

    offline && this.Console.log('Network offline', 'Fetch Error');

    // we want to be notified if anything else is caught here
    // but do not disrupt data flow
    (!offline) && this.Console.handleError(error);

    const offlineResponse = { status: 408, data: null , ok: false };

    this.handleResponse(offlineResponse, index);
  }

  /**
   * Starts the server Job to the corresponding cid
   * Can also be used to restart a Job that is in completed state
   * @param {string} cid 
   *
   * @update  this.jobs[] based on cid
   * @fires  this.jobs[].callback   applying the cid
   * 
   * @throws  {ServerQueueError}
   */
  startJob(cid) {
    try {
      // if the cid is not a string, throw an error
      if ((typeof cid !== 'string') || (cid.length !== 24))
      throw new ServerQueueError(`An invalid server job id was provided.`, cid);

      // retrieve the with the corresponding cid
      const index = this.jobs.findIndex(job  => job.cid === cid);

      // if no jobs are retrieved, throw an error
      if (typeof this.jobs[index] === 'undefined')
      throw new ServerQueueError(`Could not find a server job with id: ${cid}`);

      let {
        status, 
        method, 
        path, 
        request, 
        headers, 
        ... rest
      } = this.jobs[index];

      // check if job is ready to be started, status 860, skip if not ready
      // proceed also if status is 862 complete, allowing the job to run again
      // then mark job as in progress, status 861
      if (!(status === 860 || status === 862)) return;
      status = 861;

      // check the job method and start the job
      switch (method) {
        case 'GET':
          this.get(path)
          .then(response => this.handleResponse(response, index))
          .catch(error => this.handleFetchError(error, index));
        break;
        case 'POST': 
        case 'PATCH': 
        case 'PUT': 
        case 'DELETE': 
          this.post(path, request, headers, method)
          .then(response => this.handleResponse(response, index))
          .catch(error => this.handleFetchError(error, index));
        break;
        default:
        throw new ServerQueueError(
          'Could not initiate server job based on method supplied.'
        );
      }

      this.jobs[index] = {status, method, path, request, headers, ...rest};

    } catch (error) {
      this.Console.handleError(error);
    }
  }


  /**
   * Purge one or all server jobs
   * Provide a cid to only purge that particular job
   * @param {string} cid 
   * 
   * @return  {null}
   * 
   * @throws  {ServerQueueError}
   */
  purgeJobs(cid = null) {
    // if no cid is specified, purge all jobs
    if (cid === null) {
      this.Console.log('Purging all Server Queue jobs.', 'Purge');
      this.jobs = [];
      return;
    }

    // purge only one job
    try {
      // if the cid is not a string, throw an error
      if ((typeof cid !== 'string') || (cid.length !== 24))
      throw new ServerQueueError(`An invalid server job id was provided.`);

      // look for the job identified by cid
      const foundJobIdx = this.jobs.findIndex(job => {
        return job.cid === cid;
      });

      if (typeof foundJobIdx === 'undefined') {
        this.Console.log(`Job not found. CID: ${cid}`, 'Purge');
        return;
      }

      // delete the identified job
      this.jobs.splice(foundJobIdx, 1);
      this.Console.log(`Server job purged. CID: ${cid}`, 'Purge');
    } catch (error) {
      this.Console.handleError(error);
    }
  }

  /**
   * Retrieve the response of a particular server job identified by cid
   * @param {string} cid 
   * 
   * @return  {object}  response: { status, data, ok }
   * 
   * @throws  {ServerQueueError}
   * 
   * @note  Job not found: { status: 410, data: null }
   */
  retrieveResponse(cid) {
    try {
      // if the cid is not a string, throw an error
      if ((typeof cid !== 'string') || (cid.length !== 24))
      throw new ServerQueueError(`An invalid server job id was provided.`);

      // default response
      // 410: Gone, no longer available
      let status = 410;
      let data = null;
      let ok = false;

      // look for the job identified by cid
      const foundJob = this.jobs.find(job => job.cid === cid);

      // log a problem if job is not found
      if (typeof foundJob === 'undefined')
      this.Console.log(`Job not found. CID: ${cid}`, 'Retrieve Response');
      
      // if everything is good, return the job response status and data
      foundJob && ({status, data, ok} = foundJob.response);

      return { status, data, ok };
    } catch (error) {
      this.Console.handleError(error);
    }
  }

}

export {
  Server as default 
};