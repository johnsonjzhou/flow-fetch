/**
 * @deprecated
 * 
 * Custom DataError Error Class
 * @author Johnson Zhou <johnson@simplyuseful.io>
*/
export default class DataError extends Error {
  constructor(message = '', status = 850, scope = 'Data Error', ...params) {
    // Pass remaining arguments (including vendor specific ones) to parent constructor
    super(...params);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DataError);
    }

    this.name = 'DataError';
    // Custom debugging information
    this.status = status;
    this.scope = scope;
    this.message = message;
  }
}