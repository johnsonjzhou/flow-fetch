/**
 * ServerQueueError
 * Error indicating something has gone wrong with the Server queue.
 * @author Johnson Zhou <johnson@simplyuseful.io>
*/

export default class ServerQueueError extends Error {
  constructor(message = 'Error in Server Queue', ...params) {
    super(message, ...params);
    this.name = 'ServerQueueError';
  }
}