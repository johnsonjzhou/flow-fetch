/**
 * @deprecated 
 * 
 * Custom ServerError Error Class
 * @author Johnson Zhou <johnson@simplyuseful.io>
 *
 * @use  get || post || update methods
*/
export default class ServerError extends Error {
  constructor(message = '', status = 802, scope = 'Server Error', ...params) {
    // Pass remaining arguments (including vendor specific ones) to parent constructor
    super(...params);

    // Maintains proper stack trace for where our error was thrown (only available on V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ServerError);
    }

    this.name = 'ServerError';
    // Custom debugging information
    this.status = status;
    this.scope = scope;
    this.message = message;
  }
}